#!/usr/bin/env bash

echo "Hello Bash Installer!"

# >> To Install App:
# bash <(curl -s https://deric.ca/dddenc/setup.sh)
# or
# bash <(wget -qO- https://deric.ca/dddenc/setup.sh)

VERSION="v0.0.4"
RAND=`head -c 60 /dev/random | tr -dc 'a-zA-Z0-9'`

# >> Trap CTRL-C:
trap cleanup INT
function cleanup() {
    echo ""; echo "> Quitting..."
    #echo "Trapped CTRL-C! Cleaning up..."
    rm -Rf "/tmp/ddd-${RAND}"
    #printenv
}

sudo mkdir -p "/tmp/ddd-${RAND}"
cd "/tmp/ddd-${RAND}"


echo "> Check/Install Prerequisistes..."

# >> Check OpenSSL:
if [[ ! $(which openssl) ]]; then
    sudo apt install -y openssl
fi

# >> Check GIT:
if [[ ! $(which git) ]]; then
    sudo apt install -y git
fi

# >> Check PHP:
if [[ ! $(which php) ]]; then
    sudo apt install -y php-cli
fi


echo "> Downloading..."
sudo git clone https://dericdd@bitbucket.org/dericdd/dddenc.git

echo "> Installing ${VERSION}..."
cd dddenc
sudo git config --global advice.detachedHead false
sudo git fetch && sudo git checkout "${VERSION}"

sudo rm -f /usr/local/sbin/ddd.class.php
sudo cp ddd.class.php /usr/local/sbin/ddd.class.php 
sudo chmod 775 /usr/local/sbin/ddd.class.php

sudo rm -f /usr/local/sbin/ddd
sudo cp ddd.sh /usr/local/sbin/ddd
sudo chmod 775 /usr/local/sbin/ddd

sudo rm -f /usr/local/sbin/dddnew
sudo cp dddnew.sh /usr/local/sbin/dddnew
sudo chmod 775 /usr/local/sbin/dddnew

echo "> Clean-Up..."
sudo rm -Rf "/tmp/ddd-${RAND}"

echo "> Done."
