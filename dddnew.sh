#!/usr/bin/env bash

# >> User first passed argument as filename:
FILENAME=$1
RAND=`head -c 60 /dev/random | tr -dc 'a-zA-Z0-9'`
echo "New FileName: ${FILENAME}"
#echo "RANDOM: ${RAND}"

if [ -f $FILENAME ]; then
    echo "ERROR: Target file already exists!"
    exit 1;
fi

# >> Capture password from input:
echo "Please enter passphrase:"
read -s PASSWORD
# >> Capture password from input:
echo "Please enter passphrase again:"
read -s PASSWORD_AGAIN

if [ "${PASSWORD}" != "${PASSWORD_AGAIN}" ] 
then
    echo "ERROR: Passwords don't match!!!"
    exit 1
fi

# >> Trap CTRL-C:
trap cleanup INT
function cleanup() {
    echo ""; echo "> Quitting..."
    #echo "Trapped CTRL-C! Cleaning up..."
    rm -Rf "/tmp/ddd-${RAND}"
    #printenv
}

echo "> Generating new Vault...\n"

# >> Make TMP Dir:
mkdir -p "/tmp/ddd-${RAND}/vault"
cd "/tmp/ddd-${RAND}/vault"

# > Add Sample File:
echo "These are the guts of the your vault. Tread lightly..." > hello.txt

# > Make TAR File:
tar cf vault.tar *
mv vault.tar ../

# > CD to root working folder:
cd ../
rm -Rf vault

# >> Encrypt vault file:
openssl enc -aes-256-cbc -pass "pass:${PASSWORD}" < vault.tar > vault.tar.ssl
rm vault.tar

# >> Add config file:
echo '{"version": "0.0.1"}' > config.json

tar cf new.ddd *
mv new.ddd "${FILENAME}"

echo "> Done...\n"
