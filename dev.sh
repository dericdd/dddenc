#!/usr/bin/env bash

cd "$(dirname "$0")"
sudo rm -f /usr/local/sbin/ddd.class.php
sudo ln -s "$( cd "$(dirname "$0")" ; pwd -P )/ddd.class.php" /usr/local/sbin/ddd.class.php
echo "> Dev Mode Set..."
