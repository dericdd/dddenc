<?php

class ddd 
{
    private $path;
    private $rand;
    
    public  $archiveBin = "/usr/bin/file-roller";
    private $password = false;

    private $debug = false;
    private $silent = false;


    function __construct($input, $options=[]) {
        if ( !is_file($input) ) {
            throw new Exception("Invalid INPUT/File passed to ".__CLASS__."!\n");
        }

        // >> INPUT File:
        $this->path  = substr($input,0,1)=='/' ? $input : getcwd()."/{$input}";

        // >> TMP Folder Props:
        if (isset($options['rand'])) {
            $this->rand = $options['rand'];
        }
        else {
            $this->rand  = uniqid();
        }
    }

    private function load($path=false) {

        $path = $path!==false ? $path : $this->path;

        $cmd = "mkdir -p {$this->tmp} && tar -xf \"{$path}\" -C {$this->tmp}";
        if ($this->debug) { echo "- PATH: {$path}\n"; }
        if ($this->debug) { echo "- CMD: {$cmd}\n"; }
        system($cmd);
        //$pharObj = new PharData($input);
        //sleep(10);
    }

    public function open() {
        if (!$this->silent) { echo "> Opening...\n"; }

        // >> Load INPUT File:
        $this->load($this->path);

        // >> Decrypt Vault File:
        $this->decrypt($this->password, $this->tmpEnc, $this->tmpDec);

        if (!is_file($this->tmpDec)) {
            echo "ERROR: Could decrypt vault!\n";
            return false;
        }

        // > File modified time before opening it:
        clearstatcache($this->tmpDec);
        $timeBefore = filemtime($this->tmpDec);

        // >> Open Application:
        $this->openApp();
        clearstatcache($this->tmpDec);

        if (filemtime($this->tmpDec) != $timeBefore) {
            // >> Re-Encrypt the Vault Contents:
            $this->encrypt($this->password, $this->tmpDec, $this->tmpEnc);
            if (is_file($this->tmpDec)) { unlink($this->tmpDec); }

            // >> Run Closing Sequence:
            $this->save();
        }
        else {
            echo "> Archive unchanged...\n";
        }

        return $this;
    }

    private function openApp() {

        // Open Vault Contents:
        $cmd = "{$this->archiveBin} \"{$this->tmpDec}\"";
        if ($this->debug) { echo "- CMD: {$cmd}\n"; }
        system($cmd);
    }

    private function save() {
        if (!$this->silent) { echo "> Saving...\n"; }

        $filename = basename($this->path);

        $tarCmd = "cd {$this->tmp} && tar cf {$filename}.saved * && mv {$filename}.saved {$this->path}";
        if ($this->debug) { echo "- CMD: {$tarCmd}\n"; }

        system($tarCmd);

        return true;
    }

    private function cleanTmp($tmp=false) {
        if ($this->debug) { echo "- Cleaning up after ourselves\n"; }

        $tmp = is_dir($tmp) ? $tmp : $this->tmp();
        if (!is_dir($tmp)) { return false; }


        $dir = opendir($tmp);

        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $this->tmp . '/' . $file;
                if ( is_dir($full) )  rrmdir($full);
                else                  unlink($full);
            }
        }
        closedir($dir);
        rmdir($this->tmp);

        return true;
    }

    private function encrypt($password, $source, $dest) {

        $cmd = "openssl enc -aes-256-cbc -pass \"pass:{$password}\" < {$source} > {$dest}";
        if(!$this->silent) { echo "> Encrypting...\n"; }
        if ($this->debug) { echo "- CMD: {$cmd}\n"; }
        system($cmd);

        return true;
    }

    private function decrypt($password, $source, $dest) {

        $cmd = "openssl enc -aes-256-cbc -d -pass \"pass:{$password}\" < {$source} > {$dest}";
        if ($this->debug) { echo "- CMD: {$cmd}\n"; }
        system($cmd);

        return true;
    }


    public function password($input=false) {
        $this->password = !empty($input) ? $input : false;
        return $this;
    }
    public function rand($input=false) {
        $this->rand = !empty($input) ? $input : false;
        return $this;
    }
    public function debug($input=true) {
        $this->debug  = $input==true ? true : false;

        if ($this->silent==true && $this->debug==true) {
            $this->silent = false;
        }
        return $this;
    }
    public function silent($input=true) {
        $this->silent = $input==true ? true : false;
        
        if ($this->debug==true && $this->silent==true) { 
            $this->debug = false;
        }

        return $this;
    }

    private function tmp() {
        return "/tmp/ddd-".$this->rand;
    }
    private function tmpEnc() {
        return $this->tmp."/vault.tar.ssl";
    }
    private function tmpDec() {
        return "{$this->tmp}/vault.tar"; 
    }


    function __destruct() {
        $this->cleanTmp();
    }

    function __get($var) {
        switch ($var) { 
            case 'tmp':    return $this->tmp();    break;
            case 'tmpEnc': return $this->tmpEnc(); break;
            case 'tmpDec': return $this->tmpDec(); break;
        }
    }

    public static function _($input) {
        return new ddd($input);
    }
}
