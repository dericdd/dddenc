#!/usr/bin/env bash

# >> User first passed argument as filename:
FILENAME=$1
RAND=`head -c 60 /dev/random | tr -dc 'a-zA-Z0-9'`
#RAND=`head -c 20 /dev/random | base64`
#echo "RANDOM: ${RAND}"


# >> Capture password from input:
echo "Please enter passphrase:"
read -s PASSWORD

# >> Trap CTRL-C:
trap cleanup INT
function cleanup() {
    echo ""; echo "> Quitting..."
    #echo "Trapped CTRL-C! Cleaning up..."
    rm -Rf "/tmp/ddd-${RAND}"
    #printenv
}

php -r '
require_once "/usr/local/sbin/ddd.class.php"; 
ddd::_("'${FILENAME}'")
  ->password("'${PASSWORD}'")
  ->rand("'${RAND}'")
  ->debug(true)
  ->open()
;'


