#!/usr/bin/env bash

# >> CURL:
if [[ $(which curl) ]]; then
    bash <(curl -s https://deric.ca/dddenc/setup.sh)
elif [[ $(which wget) ]]; then
    bash <(wget -qO- https://deric.ca/dddenc/setup.sh)
else 
    echo "ERROR: Could not locate 'curl' or 'wget' to download installer!"
fi


